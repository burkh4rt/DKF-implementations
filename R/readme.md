Runs with R version 4.1.2 (2021-11-01) "Bird Hippie" and the packages contained
in `renv.lock`.

```
R
install.packages("renv")
renv::restore()
source("exampleUsage.R")
```

A trial run produced normalized rmse of 0.6167.
