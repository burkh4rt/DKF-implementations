Runs with Julia 1.7.2 and packages as given in the Manifest file.

Three separate runs of

```
julia --project=. exampleUsage.jl
```

produced normalized rmse of 0.6951, 0.6971, and 0.6748.
